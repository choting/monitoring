package com.fruitwishes.monitoring.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

public class restartService extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(restartService.class.getSimpleName(), "Service Stops! Oooooooooooooppppssssss!!!!");
        Intent serviceIntent = new Intent("com.fruitwishes.monitoring.name.LONGRUNSERVICE");
        serviceIntent.setPackage("com.fruitwishes.monitoring");
        context.startService(serviceIntent);
//        startService(new Intent(context, LocationServices.class));
    }
}
